<?php
/*
This is a sample local-config.php file

You may include other settings here that you only want enabled on your local development checkouts

Instructions:

- Create a copy of this file and call it 'local-config.php'
- Modify the values to match your local settings

*/

define('DB_NAME', 'threepartnership');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
define('WP_DEBUG', true);
define('WP_HOME','http://threepartnership.test');
define('WP_SITEURL','http://threepartnership.test');