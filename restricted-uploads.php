<?php
/**
 * Created by PhpStorm.
 * User: adam
 * Date: 09/10/2018
 * Time: 10:43
 */

require_once("wp-load.php");

if ( is_user_logged_in() && isset($_GET['redirect_to']) ) {

    $file = dirname(__FILE__).urldecode($_GET["redirect_to"]); // Decode URL-encoded string

    // Process download
    if(file_exists($file)) {
        // assume you have a full path to file stored in $filename
        if (!is_file($file)) {
            die('The file appears to be invalid.');
        }

        $filepath = str_replace('\\', '/', realpath($file));
        $filesize = filesize($filepath);
        $filename = substr(strrchr('/'.$filepath, '/'), 1);
        $extension = strtolower(substr(strrchr($filepath, '.'), 1));
        $mime = filetype($file);

        header('Content-Type: '.$mime);
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.sprintf('%d', $filesize));
        header('Expires: 0');

        // check for IE only headers
        if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false)) {
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Pragma: no-cache');
        }

        $handle = fopen($filepath, 'rb');
        fpassthru($handle);
        fclose($handle);
        exit;
    }
} else {
    auth_redirect();
}
