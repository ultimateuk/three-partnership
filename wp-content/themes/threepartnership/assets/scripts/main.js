/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
          $('#headCarousel .item:first-of-type').addClass('active');
          $('#headCarousel .carousel-indicators li:first-of-type').addClass('active');

          $('#whatclientssay .item:first-of-type').addClass('active');
          $('#whatclientssay .carousel-indicators li:first-of-type').addClass('active');

          if ($('.main-text-area').hasClass('with-image')) {
            $('.main-text-area:first').css('margin-top', '-250px');
            $('.main-text-area:first').css('opacity', '1');
            $('.main-text-area:first .text-area-content').css('padding-top', '250px');
          }

          $(window).scroll( function(){
              $('.hideme').each( function(i){
                  var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                  var bottom_of_window = $(window).scrollTop() + $(window).height();
                  if( bottom_of_window > bottom_of_object ){
                      $(this).animate({'opacity':'1'},500);
                  }
              });
          });

          $('.clients-carousel').slick({
              infinite: true,
              arrows: false,
              dots: true,
              slidesToShow: 4,
              slidesToScroll: 4,
              autoplay: true,
              autoplaySpeed: 6000,
              responsive: [
                  {
                      breakpoint: 760,
                      settings: {
                          infinite: true,
                          arrows: false,
                          dots: false,
                          slidesToShow: 1,
                          slidesToScroll: 1,
                          autoplay: true,
                          autoplaySpeed: 2000,
                      }
                  }
              ]
          });

          $('.clients-list-content .fa-times').click(function() {
              $('.client-popup').fadeOut();
              $('.background-out').fadeOut();
          });

          $('.toggle-popup .button').click(function() {
              var id = $(this).attr('data-id');
              $('.client-popup[data-id='+id+']').fadeIn();
              $('.background-out').fadeIn();
          });

          $('.menu-open').click(function() {
              $('.nav-primary').fadeIn();
          });

          $('.menu-close').click(function() {
              $('.nav-primary').fadeOut();
          });


          if ($(window).width() > 760) {
              $('.single-client').click(function () {
                  var target = $('.clients-list-content');
                  $('html, body').animate({
                      scrollTop: ($(target).offset().top - 200)
                  }, 1000);
              });
          } else {
          }

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
