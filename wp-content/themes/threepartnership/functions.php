<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

// Team custom post type
add_action('init', 'register_team');
function register_team()
{
    $labels = array(
        'name'               => 'Team',
        'singular_name'      => 'Team',
        'add_new'            => 'Add Team Member',
        'add_new_item'       => 'Add Team Member',
        'edit_item'          => 'Edit Team Member',
        'new_item'           => 'New Team Member',
        'all_items'          => 'All Team Members',
        'view_item'          => 'View Team Members',
        'search_items'       => 'Search Team Members',
        'not_found'          => 'No Team Members found',
        'not_found_in_trash' => 'No Team Members',
        'parent_item_colon'  => '',
        'menu_name'          => 'Team Members',
    );
    $args = array(
        'taxonomies'         => array('category'),
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => true,
        'menu_position'      => null,
        'supports'           => array('title', 'thumbnail'),
    );
    register_post_type('team', $args);
}

// Clients custom post type
add_action('init', 'register_clients');
function register_clients()
{
    $labels = array(
        'name'               => 'Clients',
        'singular_name'      => 'Clients',
        'add_new'            => 'Add a Client',
        'add_new_item'       => 'Add a Client',
        'edit_item'          => 'Edit a Client',
        'new_item'           => 'New Client',
        'all_items'          => 'All Clients',
        'view_item'          => 'View Clients',
        'search_items'       => 'Search Clients',
        'not_found'          => 'No Clients found',
        'not_found_in_trash' => 'No Clients',
        'parent_item_colon'  => '',
        'menu_name'          => 'Clients',
    );
    $args = array(
        'taxonomies'         => array('category'),
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => true,
        'menu_position'      => null,
        'supports'           => array('title', 'thumbnail'),
    );
    register_post_type('clients', $args);
}

// Disable XMLRPC
add_filter('xmlrpc_enabled', '__return_false');
remove_action('wp_head', 'rsd_link');

if (! function_exists('array_get')) {
    /**
     * Get array index with default if not set.
     *
     * @param      $array
     * @param      $key
     * @param null $default
     *
     * @return null
     */
    function array_get($array, $key, $default = null)
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }
}