<?php
/**
 * Template Name: Homepage
 */
?>

<?php include(locate_template('partials/page-headers.php')); ?>
<?php include(locate_template('partials/page-introduction.php')); ?>
<?php include(locate_template('partials/main-text-content.php')); ?>

<div class="sep-container">
    <hr class="content-sep global-width">
</div>

<div class="client-testimonials">
    <div class="client-testimonial-content global-width">
        <h2><?php the_field('testimonial_title'); ?></h2>
        <div id="whatclientssay" class="carousel slide" data-ride="carousel" data-interval="10000">
            <div class="carousel-inner" role="listbox">
                <?php
                if (have_rows('testimonials')):
                    while (have_rows('testimonials')) : the_row(); ?>

                        <div class="item">
                            <i class="fa fa-pause" aria-hidden="true"></i>
                            <?php the_sub_field('quote'); ?>
                            <i class="fa fa-pause" aria-hidden="true"></i>
                            <p class="quotee"><?php the_sub_field('quotee'); ?></p>

                            <?php if (get_sub_field('button_text')): ?>
                                <a class="button orange-trans-button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
                            <?php endif; ?>
                        </div>

                    <?php
                    endwhile;
                else :

                endif;
                ?>
            </div>
            <ol class="carousel-indicators">
                <?php
                $slideCounter = count(get_field('testimonials'));
                $whatsaycount = 0;
                if ($slideCounter > 1) {
                    $whatsaycount = 0;
                    if (have_rows('testimonials')):
                        while (have_rows('testimonials')) : the_row(); ?>
                            <li data-target="#whatclientssay" data-slide-to="<?php echo $whatsaycount; ?>"></li>
                            <?php
                            $whatsaycount++;
                        endwhile;
                    else :

                    endif;
                }
                ?>
            </ol>
        </div>
    </div>
</div>

<div class="latest-social-media">
    <div class="latest-social-media-content global-width">
        <h2><?php the_field('latest_social_media_title'); ?></h2>

        <?php require_once(get_template_directory() . "/twitteroauth/twitteroauth.php");
        $consumer_key        = 'an8ed4sMqhOgxYtDk1mlE5Ueq';                               // Put your consumer key here
        $consumer_secret     = 'zGrfs3oOrYKudTnmECxDOBvQjqUoZbMce4AWePyl6YLhKvwcBr';         // Put your consumer secret here
        $access_token        = '1428436206-pyOtnIyM4yqf14UpoxAcxEvVmQSoUNuxuE2Z8Oi';   // Put your access token here
        $access_token_secret = 'ihKE4KLHo3Qweav3xjz3qcgAHSU1bVNOAEP8ZppjnY7CD';   // Put your access token secret here
        $nb_of_tweets        = 3;                                                      // Nb of tweets to be displayed
        $include_rts         = false;                                                   // true to include RT's or false to exclude them

        $connection = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);

        $tweets = $connection->get('statuses/user_timeline',
            array('count' => $nb_of_tweets, 'include_rts' => $include_rts));

        ?>
        <div class="twitter-feed clearfix">
            <ul class="clearfix">
                <?php foreach ($tweets as $key => $tweet): ?>
                    <li>
                        <div class="tweet">
                            <?php
                            //links
                            $tweet = preg_replace("|[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]|","<a target='_blank' href=\"\\0\">\\0</a>",  $tweet->text);
                            //#
                            $tweet = preg_replace('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', '', $tweet);
                            //@
                            $tweet = preg_replace('/[@]+([A-Za-z0-9-_]+)/', '', $tweet);

                            $tweet = substr($tweet, 0, 50) . '...';

                            $link = $tweets[$key]->id;
                            ?>

                            <a class="twitter-click" target="_blank" href="https://twitter.com/statuses/<?php echo $link; ?>"><?php echo $tweet; ?></a>

                        </div>

                        <div class="bottom clearfix">
                            <div class="date">
                                <?php

                                $date = $tweets[$key]->created_at;
                                $date = strtotime($date);
                                $date = date('jS F Y', $date);

                                echo $date;

                                ?>
                            </div>
                            <div class="sharer">
                                <a target="_blank" href="http://twitter.com/share?text=https://twitter.com/statuses/<?php echo $link; ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="mailto:subject=http://twitter.com/share?text=https://twitter.com/statuses/<?php echo $link; ?>" class="link-sharer"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/link-sharer-01.svg"></a>
                            </div>
                        </div>

                    </li>
                <?php endforeach ?>
            </ul>
        </div>

    </div>
</div>
