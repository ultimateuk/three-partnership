<?php
/**
 * Template Name: Team
 */
?>

<?php include(locate_template('partials/page-headers.php')); ?>
<?php include(locate_template('partials/page-introduction.php')); ?>

<div class="team-members global-width">
    <div class="team-members-content clearfix">
        <?php $loop = new WP_Query( array(
            'post_type' => 'team',
            'posts_per_page' => -1,
            'order' => 'menu_order',
            )
        ); ?>
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>

            <div class="individual-team-member clearfix">
                <div class="profile-image" style="background: url('<?php echo $backgroundImg[0]; ?>');">
                    <div class="orange-overlay">
                        <a class="button white-button" href="<?php the_permalink(); ?>">Read Bio</a>
                    </div>
                </div>
                <p class="name"><?php the_title(); ?></p>
                <p class="position"><?php the_field('position'); ?></p>
                <hr>

                <?php if( get_field('linkedin_url') ): ?>
                    <a target="_blank" href="<?php the_field('linkedin_url'); ?>"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                <?php endif; ?>

            </div>

        <?php endwhile; wp_reset_query(); ?>
    </div>
</div>

<div class="footer-quote">
    <div class="footer-quote-content global-width">
        <i class="fa fa-pause" aria-hidden="true"></i>
        <?php the_field('footer_quote'); ?>
        <p class="company-name">The Three Partnership</p>
        <i class="fa fa-pause" aria-hidden="true"></i>

    </div>
</div>
