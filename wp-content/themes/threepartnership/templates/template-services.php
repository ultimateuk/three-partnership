<?php
/**
 * Template Name: Services
 */
?>

<?php include(locate_template('partials/page-headers.php')); ?>
<?php include(locate_template('partials/page-introduction.php')); ?>
<?php include(locate_template('partials/main-text-content.php')); ?>

<div class="services-list">
    <div class="services-list-content global-width">
        <?php
        if( have_rows('services') ):
            while ( have_rows('services') ) : the_row(); ?>

                <div class="single-services">
                    <?php
                    $image = get_sub_field('icon');
                    if( !empty($image) ): ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endif; ?>

                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php the_sub_field('text'); ?>
                </div>

            <?php endwhile;
        else :

        endif;
        ?>
    </div>
</div>

<hr>

<div class="our-clients">
    <div class="our-clients-content global-width">
        <h2>Our Clients</h2>

        <div class="clients-carousel">
            <?php $loop = new WP_Query(array(
                    'post_type' => 'clients',
                    'posts_per_page' => -1,
                    'order' => 'ASC'
               )
            ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="single-client">
                    <?php the_post_thumbnail(); ?>
                </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>

        <a class="button orange-button" href="/track-record/">Find out more</a>


    </div>
</div>