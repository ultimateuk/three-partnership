<footer class="content-info clearfix">
    <div class="footer-container clearfix">
        <div class="footer-left">
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
            endif;
            ?>

            <a href="http://www.apsco.org/" target="_blank">
                <img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/apsco.png">
            </a>

            <hr>

            <div class="copyright">
                <p>© 2018 Three Partnership</p>
                <?php wp_nav_menu(array('menu' => 'Footer Menu')); ?>
            </div>

        </div>
        <div class="footer-right">
            <img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg">
            <a href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number',
                    'options'); ?></a>
            <a href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address',
                    'options'); ?></a>

            <div class="address-one">
                <?php the_field('address_1', 'options'); ?>
            </div>
            <div class="address-two">
                <?php the_field('address_2', 'options'); ?>
            </div>

            <a target="_blank" class="ultimate-logo" href="https://ultimate-uk.com/">designed and produced by ultimate</a>
        </div>
    </div>
</footer>
