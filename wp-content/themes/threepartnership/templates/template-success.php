<?php
/**
 * Template Name: Success
 */
?>

<?php include(locate_template('partials/page-headers.php')); ?>
<?php include(locate_template('partials/page-introduction.php')); ?>

<div class="clients-list">
    <div class="clients-list-content global-width clearfix">
        <?php $loop = new WP_Query(array(
                'post_type'      => 'clients',
                'posts_per_page' => -1,
                'meta_query'     => array(
                    'has_quote' => array(
                        'key'     => 'quote',
                        'compare' => 'EXISTS',
                    ),
                ),
                'orderby'        => array(
                    'has_quote'  => 'DESC',
                    'menu_order' => 'ASC',
                ),
                'post_status'    => 'publish',
            )
        ); ?>
        <?php $county = 1; ?>
        <?php while ($loop->have_posts()) : $loop->the_post(); ?>

            <div class="single-client <?php if (get_field('quote')): ?>toggle-popup<?php endif; ?>" data-id="<?php echo $county; ?>">
                <div class="overlay">
                    <a class="button white-button" data-id="<?php echo $county; ?>">Read more</a>
                </div>
                <?php the_post_thumbnail(); ?>
            </div>

            <?php if (get_field('quote')): ?>
                <div class="client-popup clearfix" data-id="<?php echo $county; ?>">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <div class="client-popup-content clearfix">
                        <h2><?php the_title(); ?></h2>
                        <div class="quote clearfix">
                            <i class="fa fa-pause" aria-hidden="true"></i>
                            <?php the_field('quote'); ?>
                            <i class="fa fa-pause" aria-hidden="true"></i>
                        </div>
                        <div class="quotee">
                            <p><?php the_field('quotee'); ?></p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php $county++; ?>
        <?php endwhile;
        wp_reset_query(); ?>
    </div>
</div>
