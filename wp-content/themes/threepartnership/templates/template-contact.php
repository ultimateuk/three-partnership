<?php
/**
 * Template Name: Contact
 */
?>

<?php include(locate_template('partials/page-headers.php')); ?>

<div class="form-block global-width">
    <div class="form-block-content">
        <h1>Contact</h1>
        <?php echo do_shortcode('[ninja_form id=1]'); ?>
    </div>
</div>

<div class="locations">
    <div class="locations-content global-width">
        <a href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a>
        <a href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a>


        <div class="single-locations clearfix">
            <div class="location">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/man-hotel.svg">
                <h2>Manchester</h2>
                <?php the_field('address_1', 'options'); ?>
            </div>
            <div class="location">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/big-ben.svg">
                <h2>London</h2>
                <?php the_field('address_2', 'options'); ?>
            </div>
        </div>

    </div>
</div>


<div class="advertised-roles">
    <div class="advertised-roles-content global-width clearfix">
        <?php the_field('advertised_roles_title'); ?>
        <?php
        if( have_rows('logos') ):
            while ( have_rows('logos') ) : the_row(); ?>

                <div class="single-logo">
                    <?php
                    $image = get_sub_field('logo');
                    if( !empty($image) ): ?>
                        <a target="_blank" href="<?php the_sub_field('link'); ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
                    <?php endif; ?>
                </div>

            <?php endwhile;
        else :

        endif;
        ?>
    </div>
</div>
