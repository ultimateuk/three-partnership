<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/scripts/slick.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/styles/components/slick.css"/>
<header class="banner">
    <div class="container">
        <div class="logo">
            <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg"></a>
        </div>
        <i class="fa fa-bars menu-open" aria-hidden="true"></i>
        <nav class="nav-primary">
            <i class="fa fa-times menu-close" aria-hidden="true"></i>
            <?php
            if (has_nav_menu('primary_navigation')) :
                wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
            endif;
            ?>
        </nav>
    </div>
    <div class="three-icon" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/three-icon.svg');"></div>
</header>
<div class="background-out"></div>