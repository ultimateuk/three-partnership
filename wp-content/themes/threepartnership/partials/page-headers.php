<?php if (get_field('images')) : ?>
    <div class="page-headers">
        <div id="headCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
            <ol class="carousel-indicators">
                <?php
                $slideCounter = count(get_field('images'));
                $slideCount   = 0;
                if ($slideCounter > 1) {
                    if (have_rows('images')):
                        while (have_rows('images')) : the_row(); ?>
                            <li data-target="#headCarousel" data-slide-to="<?php echo $slideCount; ?>"></li>
                            <?php
                            $slideCount++;
                        endwhile;
                    else :

                    endif;
                }
                ?>
            </ol>
            <div class="carousel-inner" role="listbox">
                <?php
                if (have_rows('images')):
                    while (have_rows('images')) : the_row(); ?>
                        <div class="item">
                            <?php
                            $image       = get_sub_field('image');
                            $mobileImage = get_sub_field('mobile_image');
                            ?>
                            <div class="desktop-only">
                                <div class="item-image" style="background-image: url('<?php echo array_get($image['sizes'], 'large'); ?>');"></div>
                            </div>
                            <div class="mobile-only">
                                <div class="item-image" style="background-image: url('<?php echo array_get($mobileImage['sizes'], 'large'); ?>');"></div>
                            </div>
                            <?php if (get_sub_field('text')): ?>
                                <div class="base-overlay"></div>
                                <div class="item-content">
                                    <div class="text-container">
                                        <div class="text-content">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <?php if (get_sub_field('button_text')): ?>
                                            <a class="button orange-button" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>

                    <?php
                    endwhile;
                else :

                endif;
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>