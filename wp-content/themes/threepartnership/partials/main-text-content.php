<?php
if( have_rows('main_text_area') ):
    while ( have_rows('main_text_area') ) : the_row(); ?>

        <?php if( get_sub_field('background_colour') == 'white' ): ?>
            <div class="main-text-area hideme mta-white-bg <?php if( get_field('image') ): ?>with-image<?php endif; ?>">
        <?php endif; ?>

        <?php if( get_sub_field('background_colour') == 'dark-grey' ): ?>
            <div class="main-text-area hideme mta-dark-grey-bg <?php if( get_field('image') ): ?>with-image<?php endif; ?>">
        <?php endif; ?>

            <div class="text-area-content global-width">
                <?php the_sub_field('text_area'); ?>

                <?php if( get_sub_field('button_link') ): ?>
                    <a class="button orange-trans-button" href="<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a>
                <?php endif; ?>

            </div>
        </div>

    <?php endwhile;
else :

endif;
?>