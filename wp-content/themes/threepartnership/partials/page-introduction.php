<?php if( get_field('background_colour') == 'orange' ): ?>
    <div class="page-introduction mta-orange-bg <?php if( get_field('image') ): ?>with-image<?php endif; ?>">
<?php endif; ?>

<?php if( get_field('background_colour') == 'dark-grey' ): ?>
    <div class="page-introduction mta-dark-grey-bg <?php if( get_field('image') ): ?>with-image<?php endif; ?>">
<?php endif; ?>

<?php if( get_field('background_colour') == 'white' ): ?>
    <div class="page-introduction mta-white-bg <?php if( get_field('image') ): ?>with-image<?php endif; ?>">
<?php endif; ?>

    <div class="introduction-content global-width">
        <h1><?php the_field('title'); ?></h1>
        <?php the_field('introduction_text'); ?>

        <?php if( get_field('button_link') ): ?>
            <a class="button white-button" href="<?php the_field('button_link'); ?>"><?php the_field('button_text'); ?></a>
        <?php endif; ?>

        <?php if( get_field('image') ): ?>
            <?php
            $image = get_field('image');
            if( !empty($image) ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>