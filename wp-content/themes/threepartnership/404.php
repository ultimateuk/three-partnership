<div class="error-page">
    <div class="error-page-content global-width">
        <h2>We're sorry</h2>
        <p>The page you were looking for cannot be found.</p>

        <a class="button orange-trans-button" href="/">Back to home</a>
    </div>
</div>
