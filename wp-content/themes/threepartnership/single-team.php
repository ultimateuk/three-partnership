<div class="inner-team-member">
    <div class="main-image desktop-only" style="background-image: url(<?php the_field('main_image'); ?>);"></div>
    <div class="main-image mobile-only" style="background-image: url(<?php the_field('mobile_image'); ?>);"></div>
    <div class="main-content">
        <div class="main-content-content global-width">
            <h1><?php the_title(); ?></h1>
            <p class="position"><?php the_field('position'); ?></p>
            <a href="mailto:<?php the_field('personal_email'); ?>" class="email"><?php the_field('personal_email'); ?></a>
            <a href="tel:<?php the_field('personal_telephone'); ?>" class="telephone"><?php the_field('personal_telephone'); ?></a>

            <?php if( get_field('linkedin_url') ): ?>
                <a class="social-media" target="_blank" href="<?php the_field('linkedin_url'); ?>"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
            <?php endif; ?>

            <div class="biography">
                <p><?php the_field('biography'); ?></p>
            </div>
        </div>
    </div>
</div>


<?php if( get_field('quote') ): ?>
    <div class="footer-quote">
        <div class="footer-quote-content global-width">
            <i class="fa fa-pause" aria-hidden="true"></i>
            <p><?php the_field('quote'); ?></p>
            <p class="company-name"><?php the_title(); ?></p>
            <i class="fa fa-pause" aria-hidden="true"></i>
        </div>
    </div>
<?php endif; ?>