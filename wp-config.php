<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
    define('WP_LOCAL_DEV', true);

    // Include local settings
    include(dirname(__FILE__) . '/local-config.php');
} else {
    define('WP_LOCAL_DEV', false);

    // Production DB details
    define('DB_NAME', 'tpartner_data');
    define('DB_USER', 'tpartner_user');
    define('DB_PASSWORD', ']%WRvsp~a-H1');
    define('DB_HOST', 'localhost');

    // Disable debug mode
    define('WP_DEBUG', false);

    // Prevent updating WP or plugins via wp-admin
    define('DISALLOW_FILE_MODS',true);
    define('DISALLOW_FILE_EDIT', true);
    define('AUTOMATIC_UPDATER_DISABLED', true);
}

// ========================
// Custom Content Directory
// ========================
// define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
// define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content' );

// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
define( 'WP_MEMORY_LIMIT', '512M' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l]5[-ylUxY0(PsORqX_jp)yxTUj?HRp_nsB@K@|JBWct:hG>juXgkuuDMh#w-8$+');
define('SECURE_AUTH_KEY',  '?/3:dtn.ESrm2iaTPpCdy^Au)If1k07!>XueYrWB_pKcI6L}Ud, Ly|NGE*vEoro');
define('LOGGED_IN_KEY',    '$$PrqVMS:A/yHGQFY-Hh|mg xd;8md2|opM:1C^j04]h8E#0e[{5H^Kci]n|(E|/');
define('NONCE_KEY',        'DKd-Z(-`N2}`,Bd-MPc`t]d,W$Z?(h`mYk*.x#H,Gi~(@h5!]MnUyCA~&rLUh`aZ');
define('AUTH_SALT',        'xk|Rs|1;ju<ez+6a=opz@2NXF9)N=d6M,MtQFP 9ygji.f[_P1I8ePJ#0=[|pdts');
define('SECURE_AUTH_SALT', ')^|.p9 <<E?w%x&-H.eJ$XIH*(bUVGVCrX3kuO:@Y2%UCVqd,:}a>H|CH@a(+9}e');
define('LOGGED_IN_SALT',   '!8|8+qb|Jm*#-)n%do}w6%*,i9>je/q!)^VT~+2AyKeh>h?ODnSZ:V}{$hw8Eq(]');
define('NONCE_SALT',       '|Q`5cVx`QE9eG8:ggk@`SiN^ dT|3FcB0osGmLiR8bOBYTw`_X.Wk4+f HZ~4gf$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
error_reporting(E_ALL ^ E_DEPRECATED);